//
//  CustomTitleLabel.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import UIKit

class CustomTitleLabel: UILabel {
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        textColor = UIColor.white
        textAlignment = NSTextAlignment.center
        backgroundColor = #colorLiteral(red: 0.3201708794, green: 0.6376721263, blue: 0.795152843, alpha: 1)
        font = UIFont.boldSystemFont(ofSize: 25.0)
        
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.masksToBounds = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
