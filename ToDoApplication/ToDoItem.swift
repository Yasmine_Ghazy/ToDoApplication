//
//  File.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import Foundation
import Mapper

class ToDoItem{
    
    //MARK: - Properties
    var userId: Int
    var id: Int
    var title: String
    var completed: Bool
    
    //MARK: - Initialization
    init(userId: Int, id: Int, title: String, completed: Bool){
        self.userId = userId
        self.id = id
        self.title = title
        self.completed = completed
    }
    
    required init(map: Mapper) throws {
        try userId = map.from("userId")
        try id = map.from("id")
        try title = map.from("title")
        try completed = map.from("completed")
    }

    
}
