//
//  ViewController.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import UIKit
import SnapKit

class AllToDoVC: UIViewController {
    
    //MARK: - View Components
    lazy var tableView: UITableView = {
        let tv = UITableView()
        return tv
    }()
    
    //MARK: - Properties
    var toDoArray : [ToDoItem] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    let toDoItemCellId = "ToDoItemCellId"
    let toDoAPI = API()
    
    
    //MARK: - viewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup Layout
        setupView()
        setupConstraints()
        setupData()
    }
    
}

//MARK: - Methods
extension AllToDoVC{
    
    func setupView(){
        
        //Setting Delegtes
        tableView.delegate = self
        tableView.dataSource = self
        
        //Setup Main view
        self.title = "Todos"
        self.view.backgroundColor = #colorLiteral(red: 0.895696938, green: 0.8903717399, blue: 			0.8997904062, alpha: 1)
        
        //Setup register
        tableView.register(ToDoItemCell.self, forCellReuseIdentifier: toDoItemCellId)
        
        //Setup tableView
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.backgroundColor = UIColor.white
        view.addSubview(tableView)
        
    }
    
    func setupConstraints(){
        tableView.snp.makeConstraints{(make) -> Void in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
    }
    
}

//MARK: - UITableViewDataSource
extension AllToDoVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: toDoItemCellId, for: indexPath) as! ToDoItemCell
        cell.setCell(item: toDoArray[indexPath.row])
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
}

//MARK: - UITableViewDelegate
extension AllToDoVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = ToDoDetailsVC()
        vc.item = (tableView.cellForRow(at: indexPath)as!ToDoItemCell).item
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: - ToDoAPI request
extension AllToDoVC{
    
    func setupData(){
        toDoAPI.request(target: .getAllToDo, success: { (response) in
            // parse your data
            do {
                let arr = try response.mapJSON() as! [ToDoItem]
                
                let result = arr as NSArray
                
                if let arrayOfDic = result as? [Dictionary<String, AnyObject>] {
                    for aDic in arrayOfDic{
                        let id = (aDic["id"]as! Int)
                        let userId =  (aDic["userId"] as! Int)
                        let title = (aDic["title"] as! String)
                        let completed = (aDic["completed"] as! Bool)
                        
                        let item = ToDoItem(userId: userId, id: id, title: title, completed: completed)
                        self.toDoArray.append(item)
                    }
                }
            } catch {
                // can't parse data, show error
            }
        }, error: { (error) in
            // show error from server
            print(error)
        }, failure: { (error) in
            // show Moya error
            print(error)
        })
    }
    
}



