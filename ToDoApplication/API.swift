//
//  API.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import Foundation
import Moya

class API {
    
    //MARK: - Properties
    let provider = MoyaProvider<ToDoService>()
    
    
    //MARK: - Methods
    func request(target: ToDoService, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
        
        provider.request(target) { (result) in
            
            switch result {
            case .success(let response):
                // 1:
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    print("Succes")
                    successCallback(response)
                    
                } else {
                    // 2:
                    print("error")
                    let error = NSError(domain:"com.vsemenchenko.networkLayer", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                }
            case .failure(let error):
                // 3:
                print("Failure")
                failureCallback(error)
            }
            
        }
    }
    
}
