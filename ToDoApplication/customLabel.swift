//
//  customLabel.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import UIKit

class customLabel: UILabel {
  
    //MARK: - Initialization
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        font = UIFont(name: "ArialMT " ,size: 30.0)
        textColor = #colorLiteral(red: 0, green: 0.2684682608, blue: 0.4762560725, alpha: 1)
        
        textAlignment = NSTextAlignment.center
        backgroundColor = UIColor.white
        lineBreakMode = .byWordWrapping
        numberOfLines = 0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
