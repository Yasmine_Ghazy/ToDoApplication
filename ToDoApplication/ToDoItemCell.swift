//
//  ToDoItemCell.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import UIKit

class ToDoItemCell: UITableViewCell {
    
    //MARK: - View Components
    lazy var titleLabel: customLabel = {
        return customLabel()
    }()
    
    lazy var cellView: UIView = {
        return UIView()
    }()
    
    //MARK: - Properties
    var item: ToDoItem?{
        didSet{
            titleLabel.text = item!.title
        }
    }
    
    //MARK: - Initalization
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupCell()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

//MARK:- Methods
extension ToDoItemCell{
    
    func setupCell(){
        
        self.backgroundColor = UIColor.clear
        titleLabel.textAlignment = .left
        
        self.contentView.addSubview(cellView)
        cellView.addSubview(titleLabel)
        
    }
    
    func setupConstraints(){
        
        cellView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).inset(UIEdgeInsetsMake(0, 10, 0, 10))
        }
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(cellView).inset(UIEdgeInsetsMake(0, 10, 0, 10))
        }
        
    }
    
    func setCell(item: ToDoItem){
        self.item = item
    }
    
}

