//
//  AllToDoService.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import Foundation
import Moya

enum ToDoService{
    
    case getAllToDo
    
}

extension ToDoService: TargetType{
    
    /// The method used for parameter encoding.
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    /// The path to be appended to `baseURL` to form the full `URL`.
    /// The target's base `URL`.
    var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
            return "/todos"
    }
    /// The HTTP method used in the request.
    var method: Moya.Method {
        return .get
    }
    
    /// Provides stub data for use in testing.
    var sampleData: Data {
        return Data()
    }
    
    /// The type of HTTP task to be performed.
    var task: Task {
        
        return .request
    }
    
    /// The parameters to be encoded in the request.
    var parameters: [String : Any]? {
        return nil
    }

}

