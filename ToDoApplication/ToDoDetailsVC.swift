//
//  ToDoVC.swift
//  ToDoApplication
//
//  Created by Yasmine Ghazy on 7/26/18.
//  Copyright © 2018 Yasmine Ghazy. All rights reserved.
//

import UIKit

class ToDoDetailsVC: UIViewController {
    
    //MARK: - View Components
    lazy var mainView: UIView = {
        return UIView()
    }()
    lazy var idLabel: customLabel = {
        return customLabel()
    }()
    lazy var userIdLabel: customLabel = {
        return customLabel()
    }()
    lazy var titleLabel: customLabel = {
        return customLabel()
    }()
    lazy var completedLabel: customLabel = {
        return customLabel()
    }()
    
    lazy var id: CustomTitleLabel = {
        let lbl = CustomTitleLabel()
        lbl.text = "Id"
        return lbl
    }()
    
    lazy var userId: CustomTitleLabel = {
        let lbl = CustomTitleLabel()
        lbl.text = "userId"
        return lbl
    }()
    
    lazy var titleLbl: CustomTitleLabel = {
        let lbl = CustomTitleLabel()
        lbl.text = "titleLbl"
        return lbl
    }()
    
    lazy var completed: CustomTitleLabel = {
        let lbl = CustomTitleLabel()
        lbl.text = "Completed"
        return lbl
    }()
    
    
    lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [ self.id, self.idLabel, self.userId,self.userIdLabel, self.titleLbl,self.titleLabel, self.completed,self.completedLabel])
    
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.fill
        stackView.spacing = 10.0
        
        return stackView
        }()
    
    //MARK: - Properties
    var item: ToDoItem!
    
    //MARK: - View Controller Life Cycle
    override func viewDidLoad(){
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
        
    }
    
}

//MARK: - Methods
extension ToDoDetailsVC{
    
    func setupView(){
        
        //Setting labels
        idLabel.text = String(item.id)
        userIdLabel.text = String(item.userId)
        titleLabel.text = item.title
        completedLabel.text = String(item.completed)
        
        //Setup Main view
        view.backgroundColor = UIColor.white
        navigationController?.title = title
        
        self.view.addSubview(stackView)

    }
    
    func setupConstraints(){
        
        stackView.snp.makeConstraints{(make) -> Void in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(100, 10, 100, 10))
        }
        
    }
    
}

